package com.atlassian.bitbucket.server.suggestreviewers;

import com.atlassian.bitbucket.commit.Commit;

/**
 * Suggests appropriate reviewers for a set of {@link Commit commits}.
 *
 * @since 1.0
 */
public interface SuggestedReviewerService {

    Iterable<SuggestedReviewer> getSuggestedReviewers(PullRequestDetails pullRequestDetails, int count);

}