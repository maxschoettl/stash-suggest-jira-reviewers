package com.atlassian.bitbucket.server.suggestreviewers.internal.suggester.git;

import com.atlassian.bitbucket.i18n.I18nService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.git.command.GitCommandExitHandler;

/**
 * Custom {@link GitCommandExitHandler} for that looks for a particular error message returned by
 * {@code git-blame} when a non-existent path is specified.
 */
public class MissingPathIgnoringExitHandler extends GitCommandExitHandler {

    public MissingPathIgnoringExitHandler(I18nService i18nService, Repository repository) {
        super(i18nService, repository);
    }

    @Override
    protected void evaluateStdErr(String stdErr, String command) {
        // Check if the error message is the standard output given if the path doesn't exist - ignore it if so
        if (stdErr.startsWith("fatal: no such path")) {
            // ok, fair enough
            return;
        }

        // Note that, on GitCommandExitHandler, stderr lines that start with "warning: " are not considered
        // a failure _unless_ the exit code is non-zero or an exception was thrown by a handler
        super.evaluateStdErr(stdErr, command);
    }
}
