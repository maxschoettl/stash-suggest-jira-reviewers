package com.atlassian.bitbucket.server.suggestreviewers;

import com.atlassian.bitbucket.commit.Commit;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * The details about a pull request for which reviewers should be suggested.
 *
 * @since 2.0
 */
public class PullRequestDetails {

    private final Commit fromCommit;
    private final Commit toCommit;
    private final Commit mergeBase;
    private final String fromRefId;
    private final String toRefId;

    private PullRequestDetails(Builder builder) {
        this.fromCommit = builder.fromCommit;
        this.toCommit = builder.toCommit;
        this.mergeBase = builder.mergeBase;
        this.fromRefId = builder.fromRefId;
        this.toRefId = builder.toRefId;
    }

    /**
     * @return the latest commit that contains the changes to review and merge
     */
    @Nonnull
    public Commit getFromCommit() {
        return fromCommit;
    }

    /**
     * @return the latest commit of the ref that the changes should be merged into
     */
    @Nonnull
    public Commit getToCommit() {
        return toCommit;
    }

    /**
     * @return the merge base (common ancestor) of the from and to commits, or {@code null}
     */
    @Nullable
    public Commit getMergeBase() {
        return mergeBase;
    }

    /**
     * @return the id of the ref that contains the changes to review and merge, or {@code null}
     */
    @Nullable
    public String getFromRefId() {
        return fromRefId;
    }

    /**
     * @return the id of the ref that the changes should be merged into, or {@code null}
     */
    @Nullable
    public String getToRefId() {
        return toRefId;
    }

    /**
     * Builder for pull request details, see getters.
     */
    public static class Builder {

        private Commit fromCommit;
        private Commit toCommit;
        private Commit mergeBase;
        private String fromRefId;
        private String toRefId;

        @Nonnull
        public Builder fromCommit(@Nonnull Commit fromCommit) {
            this.fromCommit = fromCommit;
            return this;
        }

        @Nonnull
        public Builder toCommit(@Nonnull Commit toCommit) {
            this.toCommit = toCommit;
            return this;
        }

        @Nonnull
        public Builder mergeBase(@Nullable Commit mergeBase) {
            this.mergeBase = mergeBase;
            return this;
        }

        @Nonnull
        public Builder fromRefId(@Nullable String fromRefId) {
            this.fromRefId = fromRefId;
            return this;
        }

        @Nonnull
        public Builder toRefId(@Nullable String toRefId) {
            this.toRefId = toRefId;
            return this;
        }

        public PullRequestDetails build() {
            return new PullRequestDetails(this);
        }

    }
}
