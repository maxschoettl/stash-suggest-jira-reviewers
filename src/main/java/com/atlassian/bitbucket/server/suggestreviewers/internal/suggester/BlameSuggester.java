package com.atlassian.bitbucket.server.suggestreviewers.internal.suggester;

import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.i18n.I18nService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.git.GitScmConfig;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderFactory;
import com.atlassian.bitbucket.scm.git.command.GitScmCommandBuilder;
import com.atlassian.bitbucket.scm.git.command.blame.GitBlameBuilder;
import com.atlassian.bitbucket.server.suggestreviewers.PullRequestDetails;
import com.atlassian.bitbucket.server.suggestreviewers.internal.suggester.git.*;
import com.atlassian.bitbucket.server.suggestreviewers.internal.util.GitUtils;
import com.atlassian.bitbucket.server.suggestreviewers.internal.util.StringUtils;
import com.atlassian.bitbucket.server.suggestreviewers.spi.Reason;
import com.atlassian.bitbucket.server.suggestreviewers.spi.ReviewerSuggester;
import com.atlassian.bitbucket.server.suggestreviewers.spi.SimpleReason;
import com.atlassian.bitbucket.server.suggestreviewers.spi.UserResolver;
import com.atlassian.bitbucket.throttle.ThrottleService;
import com.atlassian.bitbucket.throttle.Ticket;
import com.atlassian.bitbucket.user.ApplicationUser;
import com.atlassian.fugue.Pair;
import com.google.common.base.Function;
import com.google.common.collect.*;

import java.util.*;

public class BlameSuggester implements ReviewerSuggester {

    private static final int MAX_SCORE_PER_FILE = 100;
    private static final int MAX_FILES_CONSIDERED = 5;
    private static final String SCM_COMMAND = "scm-command";

    private final GitCommandBuilderFactory builderFactory;
    private final GitScmConfig config;
    private final ThrottleService throttleService;
    private final UserResolver userResolver;
    private final I18nService i18nService;

    public BlameSuggester(GitCommandBuilderFactory builderFactory, GitScmConfig config, ThrottleService throttleService,
                          UserResolver userResolver, I18nService i18nService) {
        this.builderFactory = builderFactory;
        this.config = config;
        this.throttleService = throttleService;
        this.userResolver = userResolver;
        this.i18nService = i18nService;
    }

    @Override
    public Map<ApplicationUser, Collection<Reason>> suggestFor(PullRequestDetails pullRequestDetails) {
        Commit since = pullRequestDetails.getMergeBase();
        Commit until = pullRequestDetails.getFromCommit();
        if (since == null) {
            return Collections.emptyMap();
        }
        Multimap<ApplicationUser, SimpleReason> suggestions = HashMultimap.create();
        try (Ticket ignored = throttleService.acquireTicket(SCM_COMMAND)) {
            DiffSummary diffSummary = getDiffSummary(since, until);
            Map<String, ApplicationUser> usersByEmail = new HashMap<>();

            Iterator<Map.Entry<String, Pair<Integer, Integer>>> iterator = getPathDeltas(since, until).iterator();
            for (int i = 0; i < MAX_FILES_CONSIDERED && iterator.hasNext(); ++i) {
                Map.Entry<String, Pair<Integer, Integer>> delta = iterator.next();
                String path = delta.getKey();

                if (diffSummary.getCreatedPaths().contains(path)) {
                    // if the path was created by one of the commits being considered, there's no point looking
                    // at historical blame
                    --i;
                    continue;
                }

                Set<Map.Entry<ApplicationUser, Integer>> entries = getBlame(until.getRepository(), since.getRepository(),
                        path, since.getId(), usersByEmail).entrySet();

                if (entries.isEmpty()) {
                    // no blame = missing path, shouldn't happen but let's be defensive
                    --i;
                    continue;
                }

                for (Map.Entry<ApplicationUser, Integer> author : entries) {
                    boolean added = delta.getValue().left() > delta.getValue().right();

                    int locDelta = added ? delta.getValue().left() : delta.getValue().right();
                    int authorBlame = author.getValue();

                    if (diffSummary.getDeletedPaths().contains(path)) {
                        suggestions.put(author.getKey(), new SimpleReason(
                                "Previously contributed to files that were deleted.",
                                String.format("Authored %s %s of %s, which was deleted.",
                                        authorBlame, StringUtils.pluralize(authorBlame, "line", "lines"),
                                        lastSegment(path)), authorBlame * locDelta));
                    } else {
                        suggestions.put(
                                author.getKey(), new SimpleReason(
                                "Previously contributed to files that were modified.",
                                String.format("Authored %s %s of %s, which had %s %s %s %s it.",
                                        authorBlame, StringUtils.pluralize(authorBlame, "line", "lines"),
                                        lastSegment(path), locDelta, StringUtils.pluralize(locDelta, "line", "lines"),
                                        added ? "added" : "removed", added ? "to" : "from"),
                                        authorBlame * locDelta));
                    }
                }
            }
        }
        return scaleScores(suggestions).asMap();
    }

    private static String lastSegment(String path) {
        return path.substring(path.lastIndexOf("/") + 1);
    }

    private static Multimap<ApplicationUser, Reason> scaleScores(Multimap<ApplicationUser, SimpleReason> suggestions) {
        if (suggestions.isEmpty()) {
            return HashMultimap.create();
        }

        final int max = Collections.max(suggestions.values()).getScore();

        return Multimaps.transformValues(suggestions, new Function<SimpleReason, Reason>() {
            @Override
            public Reason apply(SimpleReason input) {
                int scaledScore = (int) (MAX_SCORE_PER_FILE * ((float) input.getScore()) / max);
                return new SimpleReason(input.getShortDescription(), input.getDescription(), scaledScore);
            }
        });
    }

    private GitScmCommandBuilder builderFor(Commit since) {
        return builderFor(since.getRepository());
    }

    private GitScmCommandBuilder builderFor(Repository repository) {
        return builderFactory.builder(repository);
    }

    private Map<ApplicationUser, Integer> getBlame(Repository repository, Repository secondary,
                                             String path, String at, Map<String, ApplicationUser> usersByEmail) {
        Map<ApplicationUser, Integer> blame = new HashMap<>();

        GitBlameBuilder builder = builderFor(repository).blame()
                .incremental()
                .file(path)
                .rev(at)
                .exitHandler(new MissingPathIgnoringExitHandler(i18nService, repository));
        GitUtils.setAlternateIfCrossRepository(builder, repository, secondary, config);

        Map<String, Integer> blameByEmail = builder.build(new BlameAttributionOutputHandler()).call();
        if (blameByEmail == null) {
            return blame; // path didn't exist @ since revision, ignore
        }

        for (Map.Entry<String, Integer> entry : blameByEmail.entrySet()) {
            ApplicationUser user;
            if (usersByEmail.containsKey(entry.getKey())) {
                user = usersByEmail.get(entry.getKey());
            } else {
                user = userResolver.resolve(entry.getKey());
                usersByEmail.put(entry.getKey(), user); // Even if the ApplicationUser is null; don't look again
            }
            if (user != null) {
                blame.put(user, entry.getValue());
            }
        }
        return blame;
    }

    private DiffSummary getDiffSummary(Commit since, Commit until) {
        DiffSummaryOutputHandler outputHandler = new DiffSummaryOutputHandler();
        GitScmCommandBuilder builder = builderFor(since).command("diff")
                .argument("--summary")
                .argument(since.getId() + ".." + until.getId());
        GitUtils.setAlternateIfCrossRepository(builder, since.getRepository(), until.getRepository(), config);
        builder.build(outputHandler).call();
        return outputHandler;
    }

    private List<Map.Entry<String, Pair<Integer, Integer>>> getPathDeltas(Commit since, Commit until) {
        GitScmCommandBuilder builder = builderFor(since).command("diff")
                .argument("--numstat")
                .argument(since.getId() + ".." + until.getId());
        GitUtils.setAlternateIfCrossRepository(builder, since.getRepository(), until.getRepository(), config);
        Map<String, Pair<Integer, Integer>> deltas = builder.build(new DiffNumstatOutputHandler()).call();
        return sortByAbsDelta(deltas);
    }

    private List<Map.Entry<String, Pair<Integer, Integer>>> sortByAbsDelta(Map<String, Pair<Integer, Integer>> pathDeltas) {
        List<Map.Entry<String, Pair<Integer, Integer>>> list = new ArrayList<>(pathDeltas.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Pair<Integer, Integer>>>() {
            @Override
            public int compare(Map.Entry<String, Pair<Integer, Integer>> a, Map.Entry<String, Pair<Integer, Integer>> b) {
                return Math.max(b.getValue().left(), Math.abs(b.getValue().right())) -
                       Math.max(a.getValue().left(), Math.abs(a.getValue().right()));
            }
        });
        return list;
    }
}
