package com.atlassian.bitbucket.server.suggestreviewers.internal.suggester.git;

import com.atlassian.bitbucket.io.LineReader;
import com.atlassian.bitbucket.io.LineReaderOutputHandler;
import com.atlassian.bitbucket.scm.CommandOutputHandler;
import com.atlassian.bitbucket.server.suggestreviewers.internal.util.IntHolder;
import com.google.common.collect.Maps;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BlameAttributionOutputHandler extends LineReaderOutputHandler implements CommandOutputHandler<Map<String, Integer>> {

    private static final Pattern COMMIT = Pattern.compile("([a-f0-9]{40}) [0-9]+ [0-9]+ ([0-9]+)");
    private static final Pattern AUTHOR_MAIL = Pattern.compile("^author-mail <(.*)>$");

    private final Map<String, IntHolder> authorLoc = Maps.newHashMap();
    private final Map<String, String> commit2Author = Maps.newHashMap();

    private String commit;
    private int unappliedLoc = -1;

    public BlameAttributionOutputHandler() {
        super("UTF-8");
    }

    @Nullable
    public Map<String, Integer> getOutput() {
        return Maps.transformValues(authorLoc, IntHolder.TO_INT);
    }

    @Override
    protected void processReader(LineReader lineReader) throws IOException {
        String line;
        while ((line = lineReader.readLine()) != null) {
            if (unappliedLoc > 0) {
                Matcher m = AUTHOR_MAIL.matcher(line);
                if (m.matches()) {
                    String author = m.group(1);
                    getHolder(author).add(unappliedLoc);
                    commit2Author.put(commit, author);
                    unappliedLoc = -1;
                }
            } else {
                Matcher m = COMMIT.matcher(line);
                if (m.matches()) {
                    commit = m.group(1);
                    String author = commit2Author.get(commit);
                    int loc = Integer.parseInt(m.group(2));
                    if (author != null) {
                        getHolder(author).add(loc);
                    } else {
                        unappliedLoc = loc;
                    }
                }
            }
        }
    }

    private IntHolder getHolder(String author) {
        IntHolder holder = authorLoc.get(author);
        if (holder == null) {
            holder = new IntHolder();
            authorLoc.put(author, holder);
        }
        return holder;
    }

}
