package com.atlassian.bitbucket.server.suggestreviewers.internal.util;

import com.google.common.base.Function;

public class IntHolder implements Comparable<IntHolder> {

    public static final Function<IntHolder, Integer> TO_INT = new Function<IntHolder, Integer>() {
        public Integer apply(IntHolder input) {
            return input.n;
        }
    };

    private int n;

    public int increment() {
        return ++n;
    }

    public int decrement() {
        return --n;
    }

    public int add(int increment) {
        return n += increment;
    }

    public int get() {
        return n;
    }

    @Override
    public int compareTo(IntHolder o) {
        return n - o.n;
    }
}
