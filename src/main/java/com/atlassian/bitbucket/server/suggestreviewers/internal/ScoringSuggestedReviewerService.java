package com.atlassian.bitbucket.server.suggestreviewers.internal;

import com.atlassian.bitbucket.auth.AuthenticationContext;
import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.commit.CommitsBetweenRequest;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionService;
import com.atlassian.bitbucket.server.suggestreviewers.PullRequestDetails;
import com.atlassian.bitbucket.user.ApplicationUser;
import com.atlassian.bitbucket.user.ApplicationUserEquality;
import com.atlassian.bitbucket.util.PageUtils;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.bitbucket.server.suggestreviewers.SuggestedReviewer;
import com.atlassian.bitbucket.server.suggestreviewers.SuggestedReviewerService;
import com.atlassian.bitbucket.server.suggestreviewers.spi.Reason;
import com.atlassian.bitbucket.server.suggestreviewers.spi.ReviewerSuggester;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class ScoringSuggestedReviewerService implements SuggestedReviewerService {

    private static final Logger log = LoggerFactory.getLogger(ScoringSuggestedReviewerService.class);

    private final AuthenticationContext authenticationContext;
    private final CommitService commitService;
    private final MergeBaseResolver mergeBaseResolver;
    private final PermissionService permissionService;
    private final PluginAccessor pluginAccessor;

    public ScoringSuggestedReviewerService(AuthenticationContext authenticationContext,
                                           CommitService commitService, MergeBaseResolver mergeBaseResolver,
                                           PermissionService permissionService, PluginAccessor pluginAccessor) {
        this.authenticationContext = authenticationContext;
        this.commitService = commitService;
        this.mergeBaseResolver = mergeBaseResolver;
        this.permissionService = permissionService;
        this.pluginAccessor = pluginAccessor;
    }

    @Override
    public Iterable<SuggestedReviewer> getSuggestedReviewers(PullRequestDetails details, int count) {
        if (alreadyMerged(details.getToCommit(), details.getFromCommit())) {
            return Collections.emptyList();
        }

        if (details.getMergeBase() == null) {
            Commit mergeBase = mergeBaseResolver.findMergeBase(details.getFromCommit(), details.getToCommit());

            details = new PullRequestDetails.Builder()
                    .fromCommit(details.getFromCommit())
                    .toCommit(details.getToCommit())
                    .fromRefId(details.getFromRefId())
                    .toRefId(details.getFromRefId())
                    .mergeBase(mergeBase)
                    .build();
        }

        Multimap<ApplicationUser, String> reasons = HashMultimap.create();
        Map<ApplicationUser, Reason> mostRelevantReason = new HashMap<>();
        Map<ApplicationUser, RankedUser> rankings = new HashMap<>();

        for (ReviewerSuggester suggester : pluginAccessor.getEnabledModulesByClass(ReviewerSuggester.class)) {
            try {
                Map<ApplicationUser, Collection<Reason>> suggestions = suggester.suggestFor(details);
                if (suggestions == null || suggestions.isEmpty()) {
                    // guard against suggester implementations returning unexpected output
                    continue;
                }

                for (Map.Entry<ApplicationUser, Collection<Reason>> entry : suggestions.entrySet()) {
                    if (entry.getKey() == null || entry.getValue() == null || entry.getValue().isEmpty()) {
                        // guard against suggester implementations returning unexpected output
                        continue;
                    }

                    ApplicationUser user = entry.getKey();

                    RankedUser ranking = rankings.get(user);
                    if (ranking == null) {
                        ranking = new RankedUser(user);
                        rankings.put(user, ranking);
                    }

                    Reason best = mostRelevantReason.get(user);
                    for (Reason reason : entry.getValue()) {
                        reasons.put(user, reason.getDescription());
                        ranking.add(reason.getScore());
                        if (best == null || reason.getScore() > best.getScore()) {
                            best = reason;
                        }
                    }
                    mostRelevantReason.put(user, best);
                }
            } catch (Exception e) {
                // continue to next suggester
                log.error(suggester.getClass() + " threw an exception (or returned unexpected data) when" +
                                                           " suggesting reviewers and was ignored.", e);
            }
        }

        if (rankings.isEmpty()) {
            return Collections.emptySet();
        }

        ApplicationUser current = authenticationContext.getCurrentUser();

        List<RankedUser> byScore = new ArrayList<>(rankings.values());
        Collections.sort(byScore);

        List<SuggestedReviewer> suggestions = new ArrayList<>();
        Iterator<RankedUser> iterator = byScore.iterator();
        while (iterator.hasNext() && count > 0) {
            ApplicationUser user = iterator.next().getUser();
            if (current != null && ApplicationUserEquality.equals(current, user)) {
                //Don't suggest the authenticated user; they can't review their own work
                continue;
            }

            if (permissionService.hasRepositoryPermission(user, details.getFromCommit().getRepository(), Permission.REPO_READ)) {
                suggestions.add(new SimpleSuggestedReviewer(user,
                        mostRelevantReason.get(user).getShortDescription(),
                        reasons.get(user)));
                count--;
            }
        }

        return suggestions;
    }

    private boolean alreadyMerged(Commit since, Commit until) {
        CommitsBetweenRequest request = new CommitsBetweenRequest.Builder(since.getRepository())
                .include(until.getId())
                .exclude(since.getId())
                .secondaryRepository(until.getRepository())
                .build();
        return commitService.getCommitsBetween(request, PageUtils.newRequest(0, 1)).getSize() == 0;
    }

}