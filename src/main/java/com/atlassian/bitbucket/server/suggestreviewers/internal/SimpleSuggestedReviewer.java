package com.atlassian.bitbucket.server.suggestreviewers.internal;

import com.atlassian.bitbucket.user.ApplicationUser;
import com.atlassian.bitbucket.server.suggestreviewers.SuggestedReviewer;

public class SimpleSuggestedReviewer implements SuggestedReviewer {

    private final ApplicationUser user;
    private final String shortReason;
    private final Iterable<String> reasons;

    public SimpleSuggestedReviewer(ApplicationUser user, String shortReason, Iterable<String> reasons) {
        this.user = user;
        this.shortReason = shortReason;
        this.reasons = reasons;
    }

    @Override
    public ApplicationUser getUser() {
        return user;
    }

    @Override
    public String getShortReason() {
        return shortReason;
    }

    @Override
    public Iterable<String> getReasons() {
        return reasons;
    }

}
