define('suggested-reviewers', [
    'jquery',
    'lodash',
    'bitbucket/util/events',
    'exports'
], function(
    $,
    _,
    events,
    exports) {

    var popups = {};
    var descriptionSelector = ".pull-request-reviewers div.description";

    // assumes popup with same id will always have same w/h/header
    function spawnPopup(id, w, h, header) {
        var popup = popups[id];
        if (!popup) {
            popup = popups[id] = new AJS.Dialog({
                width: w,
                height: h,
                id: id,
                closeOnOutsideClick: false
            });
            popup.addHeader(header);
            popup.addCancel("Close", function() {
                popup.hide();
            });
            popup.addPanel("", "", id + "-content");
        }
        popup.show();
        return $("." + id + "-content").empty();
    }

    exports.init = function() {
        events.on("bitbucket.internal.model.page-state.changed.sourceBranch", exports.refreshSuggestedReviewers);
        events.on("bitbucket.internal.model.page-state.changed.targetBranch", exports.refreshSuggestedReviewers);
        events.on("bitbucket.internal.feature.compare.form.state", function(data) {
            if (data.newForm === 'pullRequest') {
                spinSpinner();
            }
        });

        // trigger on page load, in case the sourceBranch & targetBranch were set by query params
        exports.refreshSuggestedReviewers();
    };

    exports.refreshSuggestedReviewers = function() {
        var fromRepo = $("input[name=fromRepoId]").val();
        var fromBranch = $("input[name=fromBranch]").val();

        var toRepo = $("input[name=toRepoId]").val();
        var toBranch = $("input[name=toBranch]").val();

        if (fromRepo && fromBranch && toRepo && toBranch) {
            exports.showSuggestedReviewers(fromRepo, fromBranch, toRepo, toBranch);
        }

        $("a.more-suggested-reviewers").die().live("click", function() {
            exports.showMoreSuggestedReviewers(fromRepo, fromBranch, toRepo, toBranch);
        });
    };

    var suggested = {}; // user cache
    var shortReasons = {}; // user cache

    exports.suggestReviewers = function(fromRepo, fromBranch, toRepo, toBranch, count, success, error) {
        $.ajax(AJS.contextPath() + "/rest/suggest-reviewers/1.0/by/ref",
            {
                data: {
                    fromRepoId: fromRepo,
                    from: fromBranch,
                    toRepoId: toRepo,
                    to: toBranch,
                    count: count,
                    avatarSize: 64
                },
                error: function(xhr, status, errorMsg) {
                    error(status + " " + errorMsg);
                },
                success: function(data) {
                    // filter out anyone already added to the review
                    var filterOut = $("#reviewers").val().split("|!|");
                    data = _.filter(data, function(suggestion) {
                        return filterOut.indexOf(suggestion.user.name) == -1;
                    });

                    // cache the user data for later lookup
                    _.each(data, function(suggestion) {
                        suggested[suggestion.user.name] = suggestion.user;
                        shortReasons[suggestion.user.name] = suggestion.shortReason;
                    });

                    success(data);
                }
            }
        );
    };

    exports.showSuggestedReviewers = function(fromRepo, fromBranch, toRepo, toBranch) {
        var $content = $(descriptionSelector);

        $content.html("<span>Suggestions: </span><div class='description-spinner'></div>");
        spinSpinner();

        exports.suggestReviewers(fromRepo, fromBranch, toRepo, toBranch, 20, function(data) {
            $content.html($(bitbucket.server.suggest.reviewers.oneLine({
                suggestedReviewers: data.slice(0, Math.min(4, data.length)),
                more: false
                // Disable the "more" view until we make the more dialog prettier
                // more: data.length >= 4
            })));
        }, function(errorMsg) {
            $content.text("Failed to load suggested reviewers: " + errorMsg)
        });
    };

    // spin the spinner if there is one
    var spinSpinner = function() {
        $(descriptionSelector).find(".description-spinner").spin("small");
    };

    // adds a reviewer to the select2 form input
    var addReviewer = function() {
        var $this = $(this);
        var username = $this.attr("data-name");

        var select2 = $('.field-group.pull-request-reviewers #reviewers').data('select2');
        var reviewers = select2.data();

        //check we don't already exist
        if (!_.some(reviewers, function(r) {return r.id === username})) {
            reviewers.push({
                id: username,
                item: suggested[username],
                text: username
            });
            select2.data(reviewers);
        }

        if ($this.hasClass("aui-button")) {
            // button on dialog
            $this.closest("tr").fadeOut();
        } else {
            // one-line link
            $this.next("span.comma").remove();
            $this.hide(); // shipit hack to workaround hover bug
        }

        // load more reviewers if we've just added the last one
        if ($(".add-suggested-reviewer:visible").length === 0) {
            $(".tipsy").hide(); // another hack to clear visible tooltips
            exports.refreshSuggestedReviewers();
        }
    };

    exports.showMoreSuggestedReviewers = function(fromRepo, fromBranch, toRepo, toBranch) {
        var $content = spawnPopup("suggested-reviewers", 800, 600, "Suggested Reviewers");
        $content.spin('large');
        exports.suggestReviewers(fromRepo, fromBranch, toRepo, toBranch, 20, function(data) {
            $content.empty().html(bitbucket.server.suggest.reviewers.dialogContent({
                suggestedReviewers: data
            }));
        }, function(error) {
            $content.empty().text(error);
        });
        $content.on("click", ".detailed-add-suggested-reviewer", addReviewer);
    };

    $(descriptionSelector).on("click", ".add-suggested-reviewer", addReviewer);

    $(".add-suggested-reviewer").tooltip({
        title: function() {
            var username = $(this).attr("data-name");
            return shortReasons[username];
        },
        live: true
    });

});

AJS.$(document).ready(function($) {
    return function() {
        require("suggested-reviewers").init();
    };
}(AJS.$));